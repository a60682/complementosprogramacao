public class Circle extends Shape {
	private double radius;
	
	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public Circle (Point origin, double radius) {
		super(origin);
		setRadius(radius);
	}
	
	public Circle () {
		super();
	}
	
	@Override
	public void Info() {
		System.out.println("Circulo: origem = " + getOrigin() + ", raio = " + getRadius());
	}

	@Override
	public double area() {
		return radius*radius*Math.PI;
	}

	@Override
	public double perimetro() {
		return 2*radius*Math.PI;
	}

}
