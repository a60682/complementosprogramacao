public class Jogo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Shape[] listaforma = new Shape[5];
		
		listaforma[0] =  new Circle();
		listaforma[1] =  new Circle(new Point(0,0), 1);
		listaforma[2] =  new Retangulo();
		listaforma[3] =  new Retangulo(new Point(1,1),10,20);
		
		listaforma[4] =  new Circle(new Point(1,3),3);
		
		for(Shape s: listaforma) {
			s.Info();
			System.out.println("A = " + s.area());
			System.out.println("B = " + s.perimetro());
		}
	}
}