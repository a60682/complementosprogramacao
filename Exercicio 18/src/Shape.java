public abstract class Shape {
	//Atributos
	private Color color;
	private Point origin;
	
	//Contrutores
	public Shape(Color color,Point origin) {
		setColor(color);
		setPoint(origin);
	}
	
	public Shape() {
		
	}
	
	public Shape(Point origin) {
		setPoint(origin);
	}
	
	//Setters
	public void setColor(Color color) {
		this.color=color;
	}
	
	public void setPoint(Point origin) {
		this.origin=origin;
	}
	
	//Getters
	public Color getColor() {
		return color;
	}
	
	public Point getOrigin() {
		return origin;
	}
	
	
	public abstract double area();
	public abstract double perimetro();
	public abstract void Info(); 
}
