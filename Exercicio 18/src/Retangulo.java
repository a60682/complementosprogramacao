public class Retangulo extends Shape {
	private double largura = 0, altura = 0;
	
	public Retangulo (Point origin, double largura, double altura) {
		super(origin);
		setLargura(largura);
		setAltura(altura);
	}
	
	public Retangulo() {
		
	}
	

	public double getLargura() {
		return largura;
	}

	public void setLargura(double largura) {
		this.largura = largura;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}
	
	@Override
	public void Info() {
		System.out.println("Circulo: origem = " + getOrigin() + ", largura = " + getLargura() + ", altura = " + getAltura());
	}

	@Override
	public double area() {
		// TODO Auto-generated method stub
		return largura*altura;
	}

	@Override
	public double perimetro() {
		// TODO Auto-generated method stub
		return largura*2+altura*2;
	}

}
