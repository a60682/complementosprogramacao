public class Color {
	private int R;
	private int G;
	private int B;
	private String nome;
	
	public Color(int R, int G, int B) {
		setR(R);
		setG(G);
		setB(B);
		setNome("");
	}
	
	public Color(int R, int G, int B, String nome) {
		setR(R);
		setG(G);
		setB(B);
		setNome(nome);
	}
	
	public void setR(int R) {
		this.R=R;
	}
	
	public void setG(int G) {
		this.G=G;
	}
	
	public void setB(int B) {
		this.B=B;
	}
	
	public void setNome(String nome) {
		this.nome=nome;
	}
	
	public int getR() {
		return R;
	}
	
	public int getG() {
		return G;
	}
	
	public int getB() {
		return B;
	}
	
	public String getNome() {
		return nome;
	}
	
}
