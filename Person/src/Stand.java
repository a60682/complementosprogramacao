public class Stand {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Person owner = new Person("Denis","09/08/1998");
		Engine engine = new Engine("Petrol",90.0f,4);
		Car c = new Car("Ford",engine,"Escort",owner);
		
		System.out.println(c.getEngine().getHorsepower());
		System.out.println(c.getOwner().getName());
		
		Car c2 =  new Car("Fiat",new Engine("Petrol",60.0f,4),"500");
		System.out.println(c2.getEngine().getHorsepower());
		System.out.println(c2.getOwner().getName());
	}

}
