public class Car {
	private String brand;
	private Engine engine;
	private String model;
	private Person owner;

	public Car(String brand, Engine engine, String model, Person owner) {
		this.setBrand(brand);
		this.setEngine(engine);
		this.setModel(model);
		this.setOwner(owner);
	}
	
	public Car(String brand, Engine engine, String model) {
		this.setBrand(brand);
		this.setEngine(engine);
		this.setModel(model);
		this.setOwner(owner);
	}

	public String getBrand() {
		return brand;
	}

	public Engine getEngine() {
		return engine;
	}

	public String getModel() {
		return model;
	}

	public Person getOwner() {
		return owner;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}
	public void setEngine(Engine engine) {
		this.engine = engine;
	}
	public void setModel(String model) {
		this.model = model;
	}
	
	public void setOwner(Person owner) {
		this.owner = owner;
	}
}
