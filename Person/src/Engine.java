public class Engine {
	private String fuel;
	private float horsepower;
	private int numberCylinders;

	public Engine(String fuel, float horsepower, int numberCylinders) {
		this.setFuel(fuel);
		this.setHorsepower(horsepower);
		this.setNumberCylinders(numberCylinders);
		
		
	}

	public String getFuel() {
		return fuel;
	}

	public float getHorsepower() {
		return horsepower;
	}

	public int getNumberCylinders() {
		return numberCylinders;
	}

	public void setFuel(String fuel) {
		this.fuel = fuel;
	}
	public void setHorsepower(float horsepower) {
		this.horsepower = horsepower;
	}
	
	public void setNumberCylinders(int numberCylinders) {
		this.numberCylinders = numberCylinders;
	}
}
