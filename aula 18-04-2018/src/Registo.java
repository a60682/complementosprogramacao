public class Registo {
	private String nome;
	private String email;
	private String genero;
	private boolean receberNewsletter;

	public Registo(String nome, String email, String genero, boolean receberNewsletter) {
		super();
		this.nome = nome;
		this.email = email;
		this.genero = genero;
		this.receberNewsletter = receberNewsletter;
	}

	public String getNome() {
		return nome;
	}

	public String getEmail() {
		return email;
	}
	
	public String getGenero() {
		return genero;
	}
	
	public boolean isReceberNewsletter() {
		return receberNewsletter;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setEmail(String email) {
		this.email = email;
	}	
	
	public void setGenero(String genero) {
		this.genero = genero;
	}
	
	public void setReceberNewsletter(boolean receberNewsletter) {
		this.receberNewsletter = receberNewsletter;
	}
}