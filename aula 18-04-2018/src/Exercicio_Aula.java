import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Component;
import javax.swing.Box;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;

public class Exercicio_Aula extends JFrame {

	private JPanel contentPane;
	private JTextField textNome;
	private JTextField textEmail;
	private JButton btnProximo;
	private JButton btnAnterior;
	private JButton btnApagar;
	private JButton btnNovo;
	private int idx = -1;
//	private int numeroRegistos = 0;

	private Vector<Registo> registos;
	private JLabel lblNewLabel;
	private JComboBox cbGenero;
	private static String listaGeneros[] = {"Masculino", "Feminino", "Indefenido"};
	private JCheckBox chckbxNewsletter;
	private JRadioButton rdbtnBenfica;
	private JRadioButton rdbtnAcademica;
	private JRadioButton rdbtnPortimonense;
	private JRadioButton rdbtnOlhanense;
	private JRadioButton rdbtnFarense;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Exercicio_Aula frame = new Exercicio_Aula();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	
	
	/**
	 * Create the frame.
	 */
	public Exercicio_Aula() {
		registos = new Vector<Registo>();
//
//		Registo r1 = new Registo("Ze", "ze@ualg.pt");
//		registos.add(r1);
//		numeroRegistos++;
//		idx = 0;
//
//		Registo r2 = new Registo("Quim", "quim@ualg.pt");
//		registos.add(r2);
//		numeroRegistos++;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 537, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		btnApagar = new JButton("Apagar");
		btnApagar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				registos.remove(idx);
				if(idx == registos.size()) {
					idx--;
				}
				fillForm();
			}
		});
		GridBagConstraints gbc_btnApagar = new GridBagConstraints();
		gbc_btnApagar.insets = new Insets(0, 0, 5, 5);
		gbc_btnApagar.gridx = 0;
		gbc_btnApagar.gridy = 2;
		contentPane.add(btnApagar, gbc_btnApagar);
		
		JLabel lblNome = new JLabel("Nome");
		GridBagConstraints gbc_lblNome = new GridBagConstraints();
		gbc_lblNome.anchor = GridBagConstraints.EAST;
		gbc_lblNome.insets = new Insets(0, 0, 5, 5);
		gbc_lblNome.gridx = 1;
		gbc_lblNome.gridy = 2;
		contentPane.add(lblNome, gbc_lblNome);
		
		textNome = new JTextField();
		textNome.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				registos.get(idx).setNome(textNome.getText());
			}
		});
		GridBagConstraints gbc_textNome = new GridBagConstraints();
		gbc_textNome.gridwidth = 7;
		gbc_textNome.insets = new Insets(0, 0, 5, 5);
		gbc_textNome.fill = GridBagConstraints.HORIZONTAL;
		gbc_textNome.gridx = 3;
		gbc_textNome.gridy = 2;
		contentPane.add(textNome, gbc_textNome);
		textNome.setColumns(10);
		
		btnAnterior = new JButton("Anterior");
		btnAnterior.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(idx==0) {
					idx = registos.size();
				}
				idx = --idx % registos.size();
				fillForm();
			}
		});
		GridBagConstraints gbc_btnAnterior = new GridBagConstraints();
		gbc_btnAnterior.insets = new Insets(0, 0, 5, 0);
		gbc_btnAnterior.gridx = 10;
		gbc_btnAnterior.gridy = 2;
		contentPane.add(btnAnterior, gbc_btnAnterior);
		
		btnNovo = new JButton("Novo");
		btnNovo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				textEmail.setEnabled(true);
				textNome.setEnabled(true);
				btnProximo.setEnabled(true);
				btnAnterior.setEnabled(true);
				btnApagar.setEnabled(true);
				cbGenero.setEnabled(true);
				chckbxNewsletter.setEnabled(true);
				
				Registo r = new Registo("..Nome..", "..Email..", listaGeneros[0], true);
				registos.add(r);
				idx = registos.size() -1;
				fillForm();
				
			}
		});
		GridBagConstraints gbc_btnNovo = new GridBagConstraints();
		gbc_btnNovo.insets = new Insets(0, 0, 5, 5);
		gbc_btnNovo.gridx = 0;
		gbc_btnNovo.gridy = 3;
		contentPane.add(btnNovo, gbc_btnNovo);
		
		JLabel lblEmail = new JLabel("Email");
		GridBagConstraints gbc_lblEmail = new GridBagConstraints();
		gbc_lblEmail.anchor = GridBagConstraints.EAST;
		gbc_lblEmail.insets = new Insets(0, 0, 5, 5);
		gbc_lblEmail.gridx = 1;
		gbc_lblEmail.gridy = 3;
		contentPane.add(lblEmail, gbc_lblEmail);
		
		textEmail = new JTextField();
		textEmail.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				registos.get(idx).setEmail(textEmail.getText());
			}
		});
		GridBagConstraints gbc_textEmail = new GridBagConstraints();
		gbc_textEmail.insets = new Insets(0, 0, 5, 5);
		gbc_textEmail.gridwidth = 7;
		gbc_textEmail.fill = GridBagConstraints.HORIZONTAL;
		gbc_textEmail.gridx = 3;
		gbc_textEmail.gridy = 3;
		contentPane.add(textEmail, gbc_textEmail);
		textEmail.setColumns(10);
		
		btnProximo = new JButton("Proximo");
		btnProximo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				idx = ++idx % registos.size();
				fillForm();
			}
		});
		GridBagConstraints gbc_btnProximo = new GridBagConstraints();
		gbc_btnProximo.insets = new Insets(0, 0, 5, 0);
		gbc_btnProximo.gridx = 10;
		gbc_btnProximo.gridy = 3;
		contentPane.add(btnProximo, gbc_btnProximo);
		
		lblNewLabel = new JLabel("Genero");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 4;
		contentPane.add(lblNewLabel, gbc_lblNewLabel);
		
		
		cbGenero = new JComboBox(listaGeneros);
		cbGenero.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				registos.get(idx).setGenero((String)cbGenero.getSelectedItem());
			}
		});
		GridBagConstraints gbc_cbGenero = new GridBagConstraints();
		gbc_cbGenero.insets = new Insets(0, 0, 5, 5);
		gbc_cbGenero.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbGenero.gridx = 3;
		gbc_cbGenero.gridy = 4;
		contentPane.add(cbGenero, gbc_cbGenero);
		
		chckbxNewsletter = new JCheckBox("Newsletter");
		chckbxNewsletter.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				registos.get(idx).setReceberNewsletter(chckbxNewsletter.isSelected());
			}
		});
		GridBagConstraints gbc_chckbxNewsletter = new GridBagConstraints();
		gbc_chckbxNewsletter.anchor = GridBagConstraints.WEST;
		gbc_chckbxNewsletter.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxNewsletter.gridx = 3;
		gbc_chckbxNewsletter.gridy = 5;
		contentPane.add(chckbxNewsletter, gbc_chckbxNewsletter);
		
		rdbtnBenfica = new JRadioButton("Benfica");
		buttonGroup.add(rdbtnBenfica);
		GridBagConstraints gbc_rdbtnBenfica = new GridBagConstraints();
		gbc_rdbtnBenfica.anchor = GridBagConstraints.WEST;
		gbc_rdbtnBenfica.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnBenfica.gridx = 3;
		gbc_rdbtnBenfica.gridy = 6;
		contentPane.add(rdbtnBenfica, gbc_rdbtnBenfica);
		
		rdbtnOlhanense = new JRadioButton("Olhanense");
		buttonGroup.add(rdbtnOlhanense);
		GridBagConstraints gbc_rdbtnOlhanense = new GridBagConstraints();
		gbc_rdbtnOlhanense.anchor = GridBagConstraints.WEST;
		gbc_rdbtnOlhanense.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnOlhanense.gridx = 4;
		gbc_rdbtnOlhanense.gridy = 6;
		contentPane.add(rdbtnOlhanense, gbc_rdbtnOlhanense);
		
		rdbtnAcademica = new JRadioButton("Academica");
		buttonGroup.add(rdbtnAcademica);
		GridBagConstraints gbc_rdbtnAcademica = new GridBagConstraints();
		gbc_rdbtnAcademica.anchor = GridBagConstraints.WEST;
		gbc_rdbtnAcademica.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnAcademica.gridx = 3;
		gbc_rdbtnAcademica.gridy = 7;
		contentPane.add(rdbtnAcademica, gbc_rdbtnAcademica);
		
		rdbtnFarense = new JRadioButton("Farense");
		buttonGroup.add(rdbtnFarense);
		GridBagConstraints gbc_rdbtnFarense = new GridBagConstraints();
		gbc_rdbtnFarense.anchor = GridBagConstraints.WEST;
		gbc_rdbtnFarense.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnFarense.gridx = 4;
		gbc_rdbtnFarense.gridy = 7;
		contentPane.add(rdbtnFarense, gbc_rdbtnFarense);
		
		rdbtnPortimonense = new JRadioButton("Portimonense");
		buttonGroup.add(rdbtnPortimonense);
		GridBagConstraints gbc_rdbtnPortimonense = new GridBagConstraints();
		gbc_rdbtnPortimonense.anchor = GridBagConstraints.WEST;
		gbc_rdbtnPortimonense.insets = new Insets(0, 0, 0, 5);
		gbc_rdbtnPortimonense.gridx = 3;
		gbc_rdbtnPortimonense.gridy = 8;
		contentPane.add(rdbtnPortimonense, gbc_rdbtnPortimonense);
		
		fillForm();
		
	}
	
	private void fillForm() {
		if(registos.size() > 0) {
			Registo r = registos.get(idx);
			
			textNome.setText(r.getNome());
			textEmail.setText(r.getEmail());
			cbGenero.setSelectedItem(r.getGenero());
			chckbxNewsletter.setSelected(r.isReceberNewsletter());
		}else {
			textEmail.setEnabled(false);
			textNome.setEnabled(false);
			textEmail.setText("");
			textNome.setText("");
			btnProximo.setEnabled(false);
			btnAnterior.setEnabled(false);
			btnApagar.setEnabled(false);
			cbGenero.setEnabled(false);
			chckbxNewsletter.setEnabled(false);
		}
	}
	

}