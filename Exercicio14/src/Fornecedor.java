public class Fornecedor extends Pessoa {
	private float valorCredito;
	private float valorSaldo;
	/**
	 * @param nome
	 * @param endereco
	 * @param telefone
	 * @param valorCredito
	 * @param valorSaldo
	 */
	public Fornecedor(String nome, String endereco, String telefone, float valorCredito, float valorSaldo) {
		super(nome, endereco, telefone);
		this.valorCredito = valorCredito;
		this.valorSaldo = valorSaldo;
	}
	public float getValorCredito() {
		return valorCredito;
	}
	public void setValorCredito(float valorCredito) {
		this.valorCredito = valorCredito;
	}
	public float getValorSaldo() {
		return valorSaldo;
	}
	public void setValorSaldo(float valorSaldo) {
		this.valorSaldo = valorSaldo;
	}
	
	
}
