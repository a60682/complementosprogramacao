public class Administrador extends Empregado {
	private float ajudaDeCusto;

	/**
	 * @param nome
	 * @param endereco
	 * @param telefone
	 * @param codigoSetor
	 * @param salarioBase
	 * @param imposto
	 * @param ajudaDeCusto
	 */
	public Administrador(String nome, String endereco, String telefone, int codigoSetor, float salarioBase,
			float imposto, float ajudaDeCusto) {
		super(nome, endereco, telefone, codigoSetor, salarioBase, imposto);
		this.ajudaDeCusto = ajudaDeCusto;
	}

	public float getAjudaDeCusto() {
		return ajudaDeCusto;
	}

	public void setAjudaDeCusto(float ajudaDeCusto) {
		this.ajudaDeCusto = ajudaDeCusto;
	}
	
}
