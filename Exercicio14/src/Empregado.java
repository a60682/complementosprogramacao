public class Empregado extends Pessoa {
	private int codigoSetor;
	private float salarioBase;
	private float imposto;
	/**
	 * @param nome
	 * @param endereco
	 * @param telefone
	 * @param codigoSetor
	 * @param salarioBase
	 * @param imposto
	 */
	public Empregado(String nome, String endereco, String telefone, int codigoSetor, float salarioBase, float imposto) {
		super(nome, endereco, telefone);
		this.codigoSetor = codigoSetor;
		this.salarioBase = salarioBase;
		this.imposto = imposto;
	}
	
	@Override
	public boolean equals(Object other) {
		return this.getEndereco().equals(((Empregado) other).getEndereco())
				&& this.getNome().equals(((Empregado) other).getNome())
				&& this.getTelefone().equals(((Empregado) other).getTelefone())		
				&& this.getSalarioBase() == ((Empregado) other).getSalarioBase()
				&& this.getCodigoSetor() == ((Empregado) other).getCodigoSetor()
				&& this.getImposto() == ((Empregado) other).getImposto();
	}
	
	public float calcularSalario() {
		return this.getSalarioBase() * (1.0f - this.getImposto());
	}
	
	public int getCodigoSetor() {
		return codigoSetor;
	}
	public void setCodigoSetor(int codigoSetor) {
		this.codigoSetor = codigoSetor;
	}
	public float getSalarioBase() {
		return salarioBase;
	}
	public void setSalarioBase(float salarioBase) {
		this.salarioBase = salarioBase;
	}
	public float getImposto() {
		return imposto;
	}
	public void setImposto(float imposto) {
		this.imposto = imposto;
	}
	
	
}
