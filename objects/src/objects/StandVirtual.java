package objects;

public class StandVirtual {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Car c = new Car();
		Car[] listarCarros = new Car[10];
		listarCarros[0] = new Car();
		listarCarros[1] = listarCarros[0];
		
		c.consumo = 8.9;
		c.cor = "vermelho";
		c.setDono("Denis");
		c.kms = 265000;
		c.marca = "Ford";
		c.modelo = "Escort";
		
		System.out.println("O " + c.getDono() + " tem um " + c.marca);
		
		listarCarros[0].consumo = 4.9;
		listarCarros[0].cor = "amarelo";
		listarCarros[0].setDono("Pedro");
		listarCarros[0].kms = 26000;
		listarCarros[0].marca = "Ford";
		listarCarros[0].modelo = "Mondeo";
	}

}
