package objects;

public class Car {
	double consumo;
	String cor;

	String dono;

	double kms;

	String marca;

	String modelo;

	public double getConsumo() {
		return consumo;
	}

	public String getCor() {
		return cor;
	}

	String getDono() {
		return dono;
	}

	public double getKms() {
		return kms;
	}

	public String getMarca() {
		return marca;
	}

	public String getModelo() {
		return modelo;
	}
	void imprimeDono() {
		System.out.println(dono);
	}
	public void setConsumo(double consumo) {
		this.consumo = consumo;
	}
	public void setCor(String cor) {
		this.cor = cor;
	}
	void setDono(String nomeDono) {
		dono = nomeDono;
	}
	
	public boolean setKms(double kms) {
		if(kms <0) {
			kms = 0;
			return false;
		}
		this.kms = kms;
		return true;
	}
	
	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
}
