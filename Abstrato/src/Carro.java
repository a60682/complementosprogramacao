public class Carro extends Vehicle {
	public Carro(int numeroRodas, String marca) {
		super(numeroRodas, marca);
	}	
	
	public void Info() {
		System.out.println("Marca " + getMarca() + " n� rodas " + getNumeroRodas() );
	}
}
