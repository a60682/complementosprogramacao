public abstract class Vehicle {
	private int numeroRodas;
	private String marca;
	
	public Vehicle(int numeroRodas, String marca) {
		setNumeroRodas(numeroRodas);
		setMarca(marca);
	}
	
	public void setNumeroRodas(int numeroRodas) {
		this.numeroRodas = numeroRodas;
	}
	
	public void setMarca (String marca) {
		this.marca = marca;
	}
	
	public int getNumeroRodas() {
		return numeroRodas;
	}
	
	public String getMarca() {
		return marca;
	}
	
	public abstract void Info();
}
