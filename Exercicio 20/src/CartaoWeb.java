public abstract class CartaoWeb {
	private String destinatario;
	private String mensagem;
	
	/**
	 * @param destinatario
	 * @param mensagem
	 */
	public CartaoWeb(String destinatario, String mensagem) {
		super();
		setDestinatario(destinatario);
		setMensagem(mensagem);
	}
	
	public String getDestinatario() {
		return destinatario;
	}
	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	public abstract void showMessage();
	
}
