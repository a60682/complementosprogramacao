import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JTextField;
import java.awt.GridBagConstraints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.Insets;
import javax.swing.JButton;

public class Exercicio25 extends JFrame {

	private JPanel contentPane;
	private JTextField txtField;
	private JTextField txtField2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Exercicio25 frame = new Exercicio25();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Exercicio25() {
		setTitle("example");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		txtField = new JTextField();
		txtField.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				setTitle("Selecione algum texto");
			}
			@Override
			public void focusLost(FocusEvent e) {
				setTitle("CP-STI");
			}
		});
		txtField.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if(txtField.getSelectedText()!=null) {
				System.out.println(txtField.getSelectedText());
				}
			}
		});
		GridBagConstraints gbc_txtField = new GridBagConstraints();
		gbc_txtField.insets = new Insets(0, 0, 5, 0);
		gbc_txtField.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtField.gridx = 0;
		gbc_txtField.gridy = 0;
		contentPane.add(txtField, gbc_txtField);
		txtField.setColumns(10);
		
		txtField2 = new JTextField();
		GridBagConstraints gbc_txtField2 = new GridBagConstraints();
		gbc_txtField2.insets = new Insets(0, 0, 5, 0);
		gbc_txtField2.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtField2.gridx = 0;
		gbc_txtField2.gridy = 1;
		contentPane.add(txtField2, gbc_txtField2);
		txtField2.setColumns(10);
		
		JButton btnTrocar = new JButton("Trocar");
		btnTrocar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String s1 = txtField.getText();
				String s2 = txtField2.getText();
				txtField.setText(s2);
				txtField2.setText(s1);
			}
		});
		GridBagConstraints gbc_btnTrocar = new GridBagConstraints();
		gbc_btnTrocar.gridx = 0;
		gbc_btnTrocar.gridy = 2;
		contentPane.add(btnTrocar, gbc_btnTrocar);
	}

}
