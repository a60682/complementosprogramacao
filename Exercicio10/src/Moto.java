public class Moto {
	public String marca;
	public String modelo;
	public String cor;
	public int marcha;
	public int menorMarcha;
	public int maiorMarcha;
	public boolean ligada;
	public int marchaAcima = marcha+1;
	public int marchaAbaixo = marcha-1;
	public boolean ligar;
	public boolean desligar;
	@Override
	public String toString() {
		return "Moto [marca=" + marca + ", modelo=" + modelo + ", cor=" + cor + ", marcha=" + marcha + ", menorMarcha="
				+ menorMarcha + ", maiorMarcha=" + maiorMarcha + ", ligada=" + ligada + ", marchaAcima=" + marchaAcima
				+ ", marchaAbaixo=" + marchaAbaixo + ", ligar=" + ligar + ", desligar=" + desligar + "]";
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getCor() {
		return cor;
	}
	public void setCor(String cor) {
		this.cor = cor;
	}
	public int getMarcha() {
		return marcha;
	}
	public void setMarcha(int marcha) {
		this.marcha = marcha;
	}
	public int getMenorMarcha() {
		return menorMarcha;
	}
	public void setMenorMarcha(int menorMarcha) {
		this.menorMarcha = menorMarcha;
	}
	public int getMaiorMarcha() {
		return maiorMarcha;
	}
	public void setMaiorMarcha(int maiorMarcha) {
		this.maiorMarcha = maiorMarcha;
	}
	public boolean isLigada() {
		return ligada;
	}
	public void setLigada(boolean ligada) {
		this.ligada = ligada;
	}
	public int getMarchaAcima() {
		return marchaAcima;
	}
	public void setMarchaAcima(int marchaAcima) {
		this.marchaAcima = marchaAcima;
	}
	public int getMarchaAbaixo() {
		return marchaAbaixo;
	}
	public void setMarchaAbaixo(int marchaAbaixo) {
		this.marchaAbaixo = marchaAbaixo;
	}
	public boolean isLigar() {
		return ligar;
	}
	public void setLigar(boolean ligar) {
		this.ligar = ligar;
	}
	public boolean isDesligar() {
		return desligar;
	}
	public void setDesligar(boolean desligar) {
		this.desligar = desligar;
	}
	/**
	 * @param marca
	 * @param modelo
	 * @param cor
	 * @param marcha
	 * @param menorMarcha
	 * @param maiorMarcha
	 * @param ligada
	 * @param marchaAcima
	 * @param marchaAbaixo
	 * @param ligar
	 * @param desligar
	 */
	public Moto(String marca, String modelo, String cor, int marcha, int menorMarcha, int maiorMarcha, boolean ligada,
			int marchaAcima, int marchaAbaixo, boolean ligar, boolean desligar) {
		super();
		this.marca = marca;
		this.modelo = modelo;
		this.cor = cor;
		this.marcha = marcha;
		this.menorMarcha = menorMarcha;
		this.maiorMarcha = maiorMarcha;
		this.ligada = ligada;
		this.marchaAcima = marchaAcima;
		this.marchaAbaixo = marchaAbaixo;
		this.ligar = ligar;
		this.desligar = desligar;
	}
	
}
