package parte1;

public class usa2pt {
	private double dolares;
	private double euros;
	
	public usa2pt() {
		this.dolares = 1.19743;
		this.euros = 0.835121886;
	}
	
	public double getDolares(double euros) {
		return euros*this.dolares;
	}
	
	public double getEuros(double dolares) {
		return dolares*this.euros;
	}
}
