package parte1;
import java.lang.Math;

public class rad2graus {
	
	public double getRad(double graus) {
		return Math.toRadians(graus);
	}
	
	public double getGraus(double rad) {
		return Math.toDegrees(rad);
	}
	
}
