package parte1;

import java.util.Scanner;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;
import java.util.Date;

public class ConvUnidades {
	public static int[] myIntArray;
	public static void geradorLista(int tamanho, int range) {
		Random rand = new Random();
		myIntArray = new int[tamanho];
    	for (int i=0; i<tamanho; i++) {
    		int m = rand.nextInt(range) + 1;
    		myIntArray[i] = m;
    	}
    	for (int i=0; i<myIntArray.length; i++) {
    		System.out.print(myIntArray[i] + " ");
    	}
	}
	
	public static void ordenarLista() {
		 new Thread(() -> {
			 Arrays.sort(myIntArray);
		 } ).start();
	}
	
	public static void mostarListaOrdenada() {
		new Thread(() -> {
			for (int i=0; i<myIntArray.length; i++) {
	    		System.out.print(myIntArray[i] + " ");
	    	}
		} ).start();
	}

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		Scanner scanchoice = new Scanner(System.in);
		usa2pt Usa2Pt = new usa2pt();
		rad2graus Rad2Graus = new rad2graus();
		Date date = new Date();
		int argumento;
		int argumento2 = 0;
		if(args.length > 0) {
			try {
				argumento = Integer.parseInt(args[0]);

				if("dolares".equals(args[1])) {
					System.out.println("O resultado �: " + Usa2Pt.getDolares(argumento) + "$");
					System.exit(0);
				}
				else if("euros".equals(args[1])) {
					System.out.println("O resultado �: " + Usa2Pt.getEuros(argumento) + "�");
					System.exit(0);
				}
				else if("rad".equals(args[1])) {
					System.out.println("O resultado �: " + Rad2Graus.getGraus(argumento) + " graus");
					System.exit(0);
				}
				else if("graus".equals(args[1])) {
					System.out.println("O resultado �: " + Rad2Graus.getRad(argumento) + " graus");
					System.exit(0);
				}
				else if("lista".equals(args[2])) {
					argumento2 = Integer.parseInt(args[1]);
					geradorLista(argumento,argumento2);
					System.exit(0);
				}
				else if("ordenar".equals(args[0])) {
					if(myIntArray != null) {
						ordenarLista();
			        	mostarListaOrdenada();
			        	System.exit(0);
					}
					else {
						System.out.println("Tem primeiro que criar a lista!");
						System.exit(0);
					}
				}
			} catch (NumberFormatException e) {
		        System.err.println("O argumento " + args[0] + " deve ser um numero inteiro.");
		        System.exit(1);
		    }
		}
		else {
			System.out.println("Bem vindo ao conversor de unidades do Denis!");
			System.out.println("1 - Conversor moeda � -> $");
			System.out.println("2 - Conversor moeda $ -> �");
			System.out.println("3 - Conversor RAD -> GRAUS");
			System.out.println("4 - Conversor GRAUS -> RAD");
			System.out.println("5 - Criar lista de n�mero aleat�ria");
			System.out.println("6 - Ordenar e mostrar lista");
			System.out.println("7 - Sair");
			int choiceentry;
	
			do {
			    System.out.println("Insira \"1\", \"2\", \"3\", \"4\", \"5\", \"6\" ou \"7\"");
			    choiceentry = scanchoice.nextInt();
			    File cal = new File("cal.txt");
				FileWriter fileWriter = new FileWriter(cal, true);
				BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			    try {
				    if(!cal.exists()){
			            System.out.println("O ficheiro \"cal\" est� a ser criado porque ainda n�o existe!");
			            cal.createNewFile();
				    }
			    }catch(IOException e){
			    	e.printStackTrace();
			    	System.out.println("N�o foi poss�vel criar o ficheiro!");
			    }
	
			    switch (choiceentry)
			    {
			        case 1:
			            System.out.println("Insira o valor em � que quer converter: ");
			            double dolares = scanchoice.nextDouble();
			            System.out.println("O resultado �: " + Usa2Pt.getDolares(dolares) + "$");
			            
			            bufferedWriter.write(date.toString() + System.getProperty("line.separator"));
			            bufferedWriter.write("O resultado �: " + Usa2Pt.getDolares(dolares) + "$" + System.getProperty("line.separator"));
			            bufferedWriter.close();
			            break;
			        case 2: 
			        	System.out.println("Insira o valor em $ que quer converter: ");
			            double euros = scanchoice.nextDouble();
			            System.out.println("O resultado �: " + Usa2Pt.getEuros(euros) + "�");
			            
			            bufferedWriter.write(date.toString() + System.getProperty("line.separator"));
			            bufferedWriter.write("O resultado �: " + Usa2Pt.getEuros(euros) + "�" + System.getProperty("line.separator"));
			            bufferedWriter.close();
			            break;
			        case 3: 
			        	System.out.println("Insira o valor em RAD que quer converter: ");
			            double rad = scanchoice.nextDouble();
			            System.out.println("O resultado �: " + Rad2Graus.getGraus(rad) + " graus");
			            
			            bufferedWriter.write(date.toString() + System.getProperty("line.separator"));
			            bufferedWriter.write("O resultado �: " + Rad2Graus.getGraus(rad) + " graus" + System.getProperty("line.separator"));
			            bufferedWriter.close();
			            break;
			        case 4: 
			        	System.out.println("Insira o valor em GRAUS que quer converter: ");
			            double graus = scanchoice.nextDouble();
			            System.out.println("O resultado �: " + Rad2Graus.getRad(graus) + " rad");
			            
			            bufferedWriter.write(date.toString() + System.getProperty("line.separator"));
			            bufferedWriter.write("O resultado �: " + Rad2Graus.getRad(graus) + " rad" + System.getProperty("line.separator"));
			            bufferedWriter.close();
			            break;
			        case 5:
			        	System.out.println("Introduza o tamanho que deseja para a lista: ");
			        	int tamanho = scanchoice.nextInt();
			        	System.out.println("Introduza qual o n�mero m�ximo que � possivel gerar: ");
			        	int range = scanchoice.nextInt();
			        	geradorLista(tamanho,range);
			        	System.out.println(" ");
			        	
			        	bufferedWriter.write(date.toString() + System.getProperty("line.separator"));
			        	bufferedWriter.write(Arrays.toString(myIntArray) + System.getProperty("line.separator"));
			        	bufferedWriter.close();
			            break;
			        case 6:
			        	if(myIntArray != null) {
			        		ordenarLista();
				        	mostarListaOrdenada();
				        	System.out.println(" ");
				        	
				        	bufferedWriter.write(date.toString() + System.getProperty("line.separator"));
				        	bufferedWriter.write(Arrays.toString(myIntArray) + System.getProperty("line.separator"));
				        	bufferedWriter.close();
			        	}else {
			        		System.out.println("Precisa de criar a lista primeiro! Clique na op��o 5!");
			        	}
			        	
			        	break;
			        case 7:
			        	System.out.println("At� breve!");
			            break;
			        default:
			            System.out.println("Op��o inv�lida!");
			    }   
			} while (choiceentry != 7);
			scanchoice.close();
		}
	}
}