import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.FlowLayout;
import javax.swing.ImageIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Window01 extends JFrame {

	private JPanel contentPane;
	private JTextField txtName;
	private JTextField txtPronome;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window01 frame = new Window01();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Window01() {
		setTitle("1 Test");
		setIconImage(Toolkit.getDefaultToolkit().getImage(Window01.class.getResource("/com/sun/javafx/scene/control/skin/modena/HTMLEditor-Background-Color-Black.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.LIGHT_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{30, 46, 86, 46, 86, 89, 0};
		gbl_contentPane.rowHeights = new int[]{23, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JLabel lblName = new JLabel("Nome");
		lblName.setIcon(new ImageIcon(Window01.class.getResource("/javax/swing/plaf/basic/icons/JavaCup16.png")));
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.anchor = GridBagConstraints.EAST;
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.gridx = 0;
		gbc_lblName.gridy = 0;
		contentPane.add(lblName, gbc_lblName);
		
		txtName = new JTextField();
		txtName.setText("Nome");
		GridBagConstraints gbc_txtName = new GridBagConstraints();
		gbc_txtName.gridwidth = 5;
		gbc_txtName.insets = new Insets(0, 0, 5, 0);
		gbc_txtName.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtName.gridx = 1;
		gbc_txtName.gridy = 0;
		contentPane.add(txtName, gbc_txtName);
		txtName.setColumns(10);
		
		JLabel lblPronome = new JLabel("Pronome");
		GridBagConstraints gbc_lblPronome = new GridBagConstraints();
		gbc_lblPronome.anchor = GridBagConstraints.EAST;
		gbc_lblPronome.insets = new Insets(0, 0, 5, 5);
		gbc_lblPronome.gridx = 0;
		gbc_lblPronome.gridy = 1;
		contentPane.add(lblPronome, gbc_lblPronome);
		
		txtPronome = new JTextField();
		txtPronome.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if(txtPronome.getText().equals("ze")) {
					txtName.setText("Ze");
				}
			}
		});
		GridBagConstraints gbc_txtPronome = new GridBagConstraints();
		gbc_txtPronome.gridwidth = 5;
		gbc_txtPronome.insets = new Insets(0, 0, 5, 0);
		gbc_txtPronome.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPronome.gridx = 1;
		gbc_txtPronome.gridy = 1;
		contentPane.add(txtPronome, gbc_txtPronome);
		txtPronome.setColumns(10);
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println(txtName.getText());
				System.out.println(txtPronome.getText());
			}
		});
		GridBagConstraints gbc_btnSubmit = new GridBagConstraints();
		gbc_btnSubmit.gridwidth = 2;
		gbc_btnSubmit.insets = new Insets(0, 0, 0, 5);
		gbc_btnSubmit.anchor = GridBagConstraints.NORTHWEST;
		gbc_btnSubmit.gridx = 1;
		gbc_btnSubmit.gridy = 2;
		contentPane.add(btnSubmit, gbc_btnSubmit);
		
		
		//MADE BY ME!!
		String s = JOptionPane.showInputDialog("Quantos queres?");
		System.out.println(s);
		
		int op = JOptionPane.showConfirmDialog(this, "Concordas?");
		if (op == JOptionPane.CANCEL_OPTION) {
			System.out.println("cancel");
		}else {
			if(op == JOptionPane.YES_OPTION) {
				System.out.println("Yes");
			}else {
				if(op == JOptionPane.NO_OPTION) {
					System.out.println("No");
				}
			}
		}
	}

}
