
public class G {
	private int numeroVertices = 1;
	private int numeroArestas = 0; 
	private int[][] matrizAdjacente;
	
	public G (int numeroVertices) {
		setNumeroVertices(numeroVertices);
		matrizAdjacente = new int[numeroVertices][numeroVertices];
	}
	
	public int getNumeroVertices() {
		return numeroVertices;
	}

	public void setNumeroVertices(int numeroVertices) {
		if(numeroVertices > 0) {
			this.numeroVertices = numeroVertices;
		}
		
	}
	
	public boolean arestaValida(int v1, int v2 ) {
		return v1>=0 && v1>=getNumeroVertices() && v2>=0 && v2>=getNumeroVertices();
	}
	
	public boolean adicionaAresta(int v1, int v2) {
		if(arestaValida(v1,v2)) {
			matrizAdjacente[v1][v2] = 1;
			matrizAdjacente[v2][v1] = 1;
			return true;
		}else {
			return false;
		}
		
	}
	
	public boolean removeAresta(int v1, int v2) {
		if(arestaValida(v1,v2)) {
			matrizAdjacente[v1][v2] = 0;
			matrizAdjacente[v2][v1] = 0;
			return true;
		}else {
			return false;
		}
		
	}
	
	public boolean eAresta(int v1, int v2) {
		if(arestaValida(v1,v2)) {
			return matrizAdjacente[v1][v2] == 1 || matrizAdjacente[v2][v1] == 1;
		}
		return false;
	}
}
