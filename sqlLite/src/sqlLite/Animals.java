package sqlLite;

public class Animals {
	private int animalId;
	private String animalName;
	private int ownerId;
	/**
	 * @param animalId
	 * @param animalName
	 * @param ownerId
	 */
	public Animals(int animalId, String animalName, int ownerId) {
		super();
		this.animalId = animalId;
		this.animalName = animalName;
		this.ownerId = ownerId;
	}
	
	
	public int getAnimalId() {
		return animalId;
	}
	public void setAnimalId(int animalId) {
		this.animalId = animalId;
	}
	
	
	public String getAnimalName() {
		return animalName;
	}
	public void setAnimalName(String animalName) {
		this.animalName = animalName;
	}
	
	
	public int getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(int ownerId) {
		this.ownerId = ownerId;
	}


	@Override
	public String toString() {
		return animalName;
	}
	
	
	
	
}
