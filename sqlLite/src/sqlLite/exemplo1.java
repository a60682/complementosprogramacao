package sqlLite;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JComboBox;
import java.awt.GridBagConstraints;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.Insets;
import javax.swing.JList;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class exemplo1 extends JFrame {

	private JPanel contentPane;
	
	static final String JDBC_DRIVER = "org.sqlite.JDBC";
	static final String DB_URL = "jdbc:sqlite:store.sqlite";
	private static Connection conn = null;
	private static Statement stmt = null;
	
	static private JComboBox ownerCB;
	static private JList<Animals> animalList;
	static private DefaultListModel<Animals> animals;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		try {
			//STEP 1: Register JDBC driver
			Class.forName(JDBC_DRIVER);
			
			//STEP 2: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL);
			
			//STEP 3: Execute a query
			System.out.println("Quering database...");
			
			stmt = conn.createStatement();
			
//			stmt.executeUpdate("INSERT INTO Animals('animalName','ownerId') VALUES ('Ant�nio',1)");
//			
//			ResultSet rs = stmt.executeQuery("SELECT * FROM Animals");
//			
//			while(rs.next()) {
//				System.out.println(rs.getString("animalName"));
//			}
//			System.out.println("Database queried succesfully...");
		}catch(SQLException se) {
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e) {
			//Handle errors for Class.forName
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					exemplo1 frame = new exemplo1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public exemplo1() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		ownerCB = new JComboBox();
		GridBagConstraints gbc_ownerCB = new GridBagConstraints();
		gbc_ownerCB.insets = new Insets(0, 0, 5, 0);
		gbc_ownerCB.fill = GridBagConstraints.HORIZONTAL;
		gbc_ownerCB.gridx = 0;
		gbc_ownerCB.gridy = 0;
		contentPane.add(ownerCB, gbc_ownerCB);
		loadOwners();
		
		JButton btnLoad = new JButton("Load");
		btnLoad.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Owner o = (Owner) ownerCB.getSelectedItem();
				System.out.println(o);
				loadAnimals(o);
			}
		});
		GridBagConstraints gbc_btnLoad = new GridBagConstraints();
		gbc_btnLoad.insets = new Insets(0, 0, 5, 0);
		gbc_btnLoad.gridx = 0;
		gbc_btnLoad.gridy = 1;
		contentPane.add(btnLoad, gbc_btnLoad);
		
		animals = new DefaultListModel<Animals>();
		animalList = new JList(animals);
		GridBagConstraints gbc_animalList = new GridBagConstraints();
		gbc_animalList.fill = GridBagConstraints.BOTH;
		gbc_animalList.gridx = 0;
		gbc_animalList.gridy = 2;
		contentPane.add(animalList, gbc_animalList);
	}
	
	static void loadOwners() {
		try {
			ResultSet rs = stmt.executeQuery("SELECT * FROM Owners");
			while(rs.next()) {
				Owner o = new Owner(rs.getInt("ownerId"),rs.getString("ownerName"));
				ownerCB.addItem(o);
			}
			System.out.println("Database queried succesfully...");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	static void loadAnimals(Owner o) {
		try {
			ResultSet rs = stmt.executeQuery("SELECT * FROM Animals WHERE ownerId = " + o.getOwnerId());
			animals.removeAllElements();
			while(rs.next()) {
				Animals a = new Animals(rs.getInt("animalId"),rs.getString("animalName"),rs.getInt("ownerId"));
				animals.addElement(a);
			}
			System.out.println("Database queried succesfully...");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
