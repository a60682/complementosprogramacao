import java.util.InputMismatchException;
import java.util.Scanner;
public class Exercicio22 {
	static Scanner s = new Scanner(System.in);
	private static int leNumero(String var) {
		while(true) {
			try{
				System.out.println(var + " = ");
				return s.nextInt();
			}catch(InputMismatchException e) {
				System.out.println("Introduza um n�mero!");
				s.next();
			}
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int x = leNumero("x");
		int y = leNumero("y");
		try {
			System.out.println("x/y = " + x/y);
		}catch(ArithmeticException e) {
			System.out.println("y = 0!!");
		}
	}

}
