public class Data {
	private int dia = 1;
	private int mes = 1;
	private int ano = 2000;
	
	public void setDia(int dia) {
		if(dataValida(dia,this.mes,this.ano)) {
			this.dia = dia;
		}else {
			throw new DataException("Dia Inv�lido!");
		}
	}
	
	public void setMes(int mes) {
		if(dataValida(this.dia,mes,this.ano)) {
			this.mes = mes;
		}else {
			throw new DataException("M�s Inv�lido!");
		}
	}
	
	public void setAno(int ano) {
		if(dataValida(this.dia,this.mes,ano)) {
			this.ano = ano;
		}else {
			throw new DataException("Ano Inv�lido!");
		}
	}
	
	public Data(int dia, int mes, int ano) {
		super();
		if(dataValida(dia, mes, ano)) {
			this.dia = dia;
			this.mes = mes;
			this.ano = ano;
		}else {
			throw new DataException("Data invalida!");
		}
	}
	
	public boolean dataValida(int dia, int mes, int ano) {
		if(mes < 1 || mes > 12 || ano == 0) {
			return false;
		}
		boolean eBissexto = (ano % 4 == 0) && ( (ano % 100 != 0) || (ano % 400 == 0));
		if(mes == 2) { //fevereiro
			if(eBissexto) {
				return (dia >= 1 && dia <= 29);
			}else {
				return (dia >= 1 && dia <= 28);
			}
		}else { //n�o � fevereiro
			if(mes == 1 || mes == 3 || mes == 5 || mes == 7 ||mes == 8 || mes == 10 || mes == 12) { //mes com 31 dias
				return (dia >= 1 && dia <= 31);
			}else { //mes com 30 dias
				return (dia >= 1 && dia <= 30);
			}
		}
	}

	@Override
	public String toString() {
		return dia + "/" + mes + "/" + ano;
	}
	
	public void adicionaDia() {
		if(dataValida(dia+1,mes,ano)) {
			setDia(dia +1);
		}else {
			setDia(1);
			if(mes==12) {
				setAno(ano+1);
				setMes(1);
			}
			setMes(mes+1);
		}
	}
	
}