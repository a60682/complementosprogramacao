import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class Exercicio24 {
	
	public static void cat (File file) throws IOException {
		RandomAccessFile input = null;
		String line = null;
		try{
			input = new RandomAccessFile(file,"r") ;
			while((line=input.readLine( )) != null) {
				System.out.println(line);
			}
			return	;
		}finally{
			if( input != null) {
				input.close() ;
			}
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			cat(new File("file.txt"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
