package exercicio7;

public class Funcionarios {
	//Defini��o das vari�veis
	String CC;
	Data dataEntrada;
	String departamento;
	String nome;
	double salario;
	String telefone;
	
	//M�todos de get
	public String getCC() {
		return CC;
	}
	
	public Data getDataEntrada() {
		return dataEntrada;
	}
	
	public String getDepartamento() {
		return departamento;
	}
	
	public String getNome() {
		return nome;
	}
	
	public double getSalario() {
		return salario;
	}
	
	public String getTelefone() {
		return telefone;
	}
	
	public double calculaGanhoAnual () {
		return this.salario * 12;
	}
	
	//M�todos de set
	public void setCC(String cC) {
		CC = cC;
	}
	
	public void setDataEntrada(Data dataEntrada) {
		this.dataEntrada = dataEntrada;
	}
	
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setSalario(double salario) {
		this.salario = salario;
	}
	
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public void recebeAumento(double aumento) {
		this.salario = this.salario + aumento;
	}

	public void mostra() {
		System.out.print("Nome: ");
		System.out.println(this.getNome());
		System.out.print("Departamento: ");
		System.out.println(this.getDepartamento());
		System.out.print("Sal�rio: ");
		System.out.println(this.getSalario());
		System.out.print("Data Entrada Banco: ");
		System.out.println(this.getDataEntrada());
		System.out.print("Cart�o Cidad�o: ");
		System.out.println(this.getCC());
		System.out.print("Telefone: ");
		System.out.println(this.getTelefone());
	}
}

