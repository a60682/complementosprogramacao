package exercicio7;

public class Data {
	int Dia;
	int Mes;
	int Ano;
	
	void setData(int dia, int mes, int ano) {
		this.Dia = dia;
		this.Mes = mes;
		this.Ano = ano;
	}

	@Override
	public String toString() {
		return "[Dia=" + Dia + ", Mes=" + Mes + ", Ano=" + Ano + "]";
	}
	
	
}
