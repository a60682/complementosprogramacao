import java.util.Date;
public interface Insurance {
	public void setDate(Date date);
	public Date getDate();
	
	public void setEnsuranceValue(float value);
	public float getEnsuranceValue();
}
