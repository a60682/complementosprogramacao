
public class Person {
	private String birthday;
	private String name;

	public Person(String name, String birthday) {
		this.setName(name);
		this.setBirthday(birthday);
	}

	public String getBirthday() {
		return birthday;
	}

	public String getName() {
		return name;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}
