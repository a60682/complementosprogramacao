public class landVeichle extends veichle {
	private float landVelocity;
	private int numberOfWheels;
	private Wheel wheels;
	
	/**
	 * @param brand
	 * @param model
	 * @param numberOfPassengers
	 * @param owner
	 * @param landVelocity
	 * @param numberOfWheels
	 * @param wheels
	 */
	public landVeichle(String brand, String model, int numberOfPassengers, Person owner, float landVelocity,
			int numberOfWheels, Wheel wheels) {
		super(brand, model, numberOfPassengers, owner);
		this.landVelocity = landVelocity;
		this.numberOfWheels = numberOfWheels;
		this.wheels = wheels;
	}
}
