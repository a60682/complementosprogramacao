public class veichle {
	private String brand;
	private String model;
	private int numberOfPassengers;
	private Person owner;

	/**
	 * @param brand
	 * @param model
	 * @param numberOfPassengers
	 * @param owner
	 */
	public veichle(String brand, String model, int numberOfPassengers, Person owner) {
		super();
		this.brand = brand;
		this.model = model;
		this.numberOfPassengers = numberOfPassengers;
		this.owner = owner;
	}

	public String getBrand() {
		return brand;
	}

	public String getModel() {
		return model;
	}

	public int getNumberOfPassengers() {
		return numberOfPassengers;
	}

	public Person getOwner() {
		return owner;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public void setNumberOfPassengers(int numberOfPassengers) {
		this.numberOfPassengers = numberOfPassengers;
	}
	
	public void setOwner(Person owner) {
		this.owner = owner;
	}
	
	
}
