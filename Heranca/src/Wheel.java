public class Wheel {
	private String size;

	/**
	 * @param size
	 */
	public Wheel(String size) {
		super();
		this.size = size;
	}
	
	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}
}
