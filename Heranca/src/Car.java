public class Car extends landVeichle implements CirculationTax, Comparable{
	private int numberOfDoors;
	private Engine engine;
	private float tax;
	
	@Override
	public int compareTo(Object o) {
		Car c = (Car)o;
		if(getNumberOfDoors()<c.getNumberOfDoors()) {
			return -1;
		}
		if(getNumberOfDoors()>c.getNumberOfDoors()) {
			return 1;
		}
		
		return 0;
	}
	
	
	
	
	@Override
	public void setTax(float tax) {
		this.tax=tax;
	}
	@Override
	public float getTax() {
		return this.tax;
	}
	
	/**
	 * @param brand
	 * @param model
	 * @param numberOfPassengers
	 * @param owner
	 * @param landVelocity
	 * @param numberOfWheels
	 * @param wheels
	 * @param numberOfDoors
	 * @param engine
	 */
	public Car(String brand, String model, int numberOfPassengers, Person owner, float landVelocity, int numberOfWheels,
			Wheel wheels, int numberOfDoors, Engine engine) {
		super(brand, model, numberOfPassengers, owner, landVelocity, numberOfWheels, wheels);
		this.numberOfDoors = numberOfDoors;
		this.engine = engine;
	}
	
	public int getNumberOfDoors() {
		return numberOfDoors;
	}

	public void setNumberOfDoors(int numberOfDoors) {
		this.numberOfDoors = numberOfDoors;
	}

	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}
	
}
